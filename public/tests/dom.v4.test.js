describe("添加样式和元素 v4", function () {
  describe("GM.addStyle", function () {
    before("支持", function () {
      if (typeof GM.addStyle != "function") {
        this.skip();
      }
    });
    it("GM.addStyle", async function () {
      const um = document.querySelector("#um").value;
      let elem = GM.addStyle("#inst { color: red; }");
      if (um === "tm") {
        chai.assert.typeOf(elem.then, "function", "应为 Promise");
        elem = await elem;
      } else {
        chai.assert.notInstanceOf(elem, Promise, "同步的，不应为 Promise");
      }
      chai.assert.strictEqual(
        getComputedStyle(document.getElementById("inst")).color,
        "rgb(255, 0, 0)",
        "没有成功应用样式",
      );
      chai.assert.instanceOf(elem, HTMLStyleElement, "返回注入的元素");
      chai.assert.strictEqual(
        elem.parentNode.tagName,
        "HEAD",
        "在 <head> 注入 <style>",
      );
    });
    it("GM.addStyle().then", function (done) {
      const um = document.querySelector("#um").value;
      if (um !== "vm") this.skip();
      const elem = GM.addStyle("#inst { color: blue; }");
      chai.assert.notInstanceOf(elem, Promise, "不应为 Promise");
      chai.assert.isFunction(elem.then, "带有 then 函数");
      elem.then(function (el) {
        chai.assert.strictEqual(
          getComputedStyle(document.getElementById("inst")).color,
          "rgb(0, 0, 255)",
          "没有成功应用样式",
        );
        chai.assert.instanceOf(el, HTMLStyleElement, "回调参数为注入的元素");
        chai.assert.strictEqual(
          el.parentNode.tagName,
          "HEAD",
          "在 <head> 注入 <style>",
        );
        done();
      });
    });
  });
  describe("GM.addElement", function () {
    before("支持", function () {
      if (typeof GM.addElement != "function") {
        this.skip();
      }
    });
    it("GM.addElement(t, a)", async function () {
      const um = document.querySelector("#um").value;
      let elem = GM.addElement("span", {
        textContent: "OK",
        style: "color: snow;",
        test: "OK",
      });
      if (um === "tm") {
        chai.assert.typeOf(elem.then, "function", "应为 Promise");
        elem = await elem;
      } else {
        chai.assert.notInstanceOf(elem, Promise, "同步的，不应为 Promise");
      }
      chai.assert.instanceOf(elem, HTMLSpanElement, "返回注入的元素");
      chai.assert.strictEqual(elem.innerText, "OK", "未设置内容");
      chai.assert.strictEqual(elem.style.cssText, "color: snow;", "未设置属性");
      chai.assert.strictEqual(
        elem.getAttribute("test"),
        "OK",
        "应为 HTML 属性，除了 textContent",
      );
      chai.assert.isUndefined(elem.test, "不应为 DOM 属性，除了 textContent");
      if (um === "vm") {
        chai.assert.strictEqual(
          elem.parentNode.tagName,
          "BODY",
          "默认在 <body> 注入元素",
        );
      } else {
        chai.assert.strictEqual(
          elem.parentNode.tagName,
          "HEAD",
          "默认在 <head> 注入元素",
        );
      }
    });
    it("GM.addElement(p, t, a)", async function () {
      const um = document.querySelector("#um").value;
      let elem = GM.addElement(document.getElementById("inst"), "span", {
        textContent: "OK",
        style: "color: snow;",
      });
      if (um === "tm") {
        chai.assert.typeOf(elem.then, "function", "应为 Promise");
        elem = await elem;
      } else {
        chai.assert.notInstanceOf(elem, Promise, "同步的，不应为 Promise");
      }
      chai.assert.instanceOf(elem, HTMLSpanElement, "返回注入的元素");
      chai.assert.strictEqual(elem.innerText, "OK", "未设置内容");
      chai.assert.strictEqual(elem.style.cssText, "color: snow;", "未设置属性");
      chai.assert.strictEqual(elem.parentNode.id, "inst", "父元素不正确");
    });
    it("GM.addElement(t, a).then", function (done) {
      const um = document.querySelector("#um").value;
      if (um !== "vm") this.skip();
      const elem = GM.addElement("span", {
        textContent: "OK",
        style: "color: snow;",
      });
      chai.assert.notInstanceOf(elem, Promise, "不应为 Promise");
      chai.assert.isFunction(elem.then, "带有 then 函数");
      elem.then(function (el) {
        chai.assert.instanceOf(el, HTMLSpanElement, "返回注入的元素");
        chai.assert.strictEqual(el.innerText, "OK", "未设置内容");
        chai.assert.strictEqual(el.style.cssText, "color: snow;", "未设置属性");
        chai.assert.strictEqual(
          el.parentNode.tagName,
          "BODY",
          "默认在 <body> 注入元素",
        );
        done();
      });
    });
    it("GM.addElement(p, t, a).then", function () {
      const um = document.querySelector("#um").value;
      if (um !== "vm") this.skip();
      const elem = GM.addElement(document.getElementById("inst"), "span", {
        textContent: "OK",
        style: "color: snow;",
      });
      chai.assert.notInstanceOf(elem, Promise, "不应为 Promise");
      chai.assert.isFunction(elem.then, "带有 then 函数");
      elem.then((el) => {
        chai.assert.instanceOf(el, HTMLSpanElement, "返回注入的元素");
        chai.assert.strictEqual(el.innerText, "OK", "未设置内容");
        chai.assert.strictEqual(el.style.cssText, "color: snow;", "未设置属性");
        chai.assert.strictEqual(el.parentNode.id, "inst", "父元素不正确");
      });
    });
  });
});
