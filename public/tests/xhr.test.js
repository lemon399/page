describe("GM_xmlhttpRequest", function () {
  before("支持", function () {
    if (typeof GM_xmlhttpRequest != "function") {
      this.skip();
    }
  });
  describe("常量", function () {
    it("状态常量", function () {
      const um = document.querySelector("#um").value;
      if (um !== "tm") this.skip();
      const stats = ["UNSENT", "OPENED", "HEADERS_RECEIVED", "LOADING", "DONE"];
      chai.assert.containsAllKeys(GM_xmlhttpRequest, stats, "不存在状态常量");
      stats.forEach((key, i) => {
        chai.assert.strictEqual(GM_xmlhttpRequest[key], i, "状态常量值不匹配");
      });
    });
    it("响应类型常量", function () {
      const um = document.querySelector("#um").value;
      if (um !== "tm") this.skip();
      ["arraybuffer", "blob", "document", "json", "stream", "text"].forEach(
        (type) => {
          chai.assert.hasAnyKeys(
            GM_xmlhttpRequest,
            `RESPONSE_TYPE_${type.toUpperCase()}`,
            `不存在响应类型常量 RESPONSE_TYPE_${type.toUpperCase()}`,
          );
          chai.assert.strictEqual(
            GM_xmlhttpRequest[`RESPONSE_TYPE_${type.toUpperCase()}`],
            type,
            "响应类型常量值不正确",
          );
        },
      );
    });
  });
  describe("基本收发", function () {
    it("默认", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get?id=LemonMocha",
        onload(...args) {
          cbd("xhr onload 默认", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.include(
              resp.responseText,
              "LemonMocha",
              "响应文本不匹配",
            );
            chai.assert.strictEqual(
              resp.finalUrl,
              "https://httpbin.org/get?id=LemonMocha",
              "最终响应地址不匹配",
            );
            chai.assert.include(
              resp.responseHeaders,
              "gunicorn/",
              "响应头内容不匹配",
            );
            chai.assert.isUndefined(
              resp.responseType,
              "响应类型未指定, 应为 undefined",
            );
            chai.assert.isString(
              resp.response,
              "响应类型未指定, 默认应为 string",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("显式 GET", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get?id=LemonMocha",
        method: "GET",
        onload(...args) {
          cbd("xhr onload GET", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.include(
              resp.responseText,
              "LemonMocha",
              "响应文本不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("URL 对象", function (done) {
      this.timeout(5000);
      const url = new URL("/get?id=LemonMocha", "https://httpbin.org");
      GM_xmlhttpRequest({
        url,
        onload(...args) {
          cbd("xhr onload URL", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.include(
              resp.responseText,
              "LemonMocha",
              "响应文本不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data: "LemonMocha",
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.strictEqual(
              JSON.parse(resp.responseText).data,
              "LemonMocha",
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST FormData string", function (done) {
      this.timeout(5000);
      const data = new FormData();
      data.set("string", "LemonMocha");
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data,
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST FormData string", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.deepEqual(
              JSON.parse(resp.responseText).form,
              { string: "LemonMocha" },
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST FormData File", function (done) {
      this.timeout(5000);
      const data = new FormData();
      data.set("mocha", new File(["LemonMocha"], "mocha.txt"));
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data,
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST FormData File", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.deepEqual(
              JSON.parse(resp.responseText).files,
              { mocha: "LemonMocha" },
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST Array", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data: ["LemonMocha", 0],
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST Array", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.strictEqual(
              JSON.parse(resp.responseText).data,
              "LemonMocha,0",
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST Blob", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data: new Blob(["lemon", "mocha"]),
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST Blob", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.strictEqual(
              JSON.parse(resp.responseText).data,
              "lemonmocha",
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST File", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data: new File(["LemonMocha"], "mocha.txt"),
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST File", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.strictEqual(
              JSON.parse(resp.responseText).data,
              "LemonMocha",
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST URLSearchParams", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um === "sc") this.skip();
      const param = new URLSearchParams();
      param.set("ok", "100");
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data: param,
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST URLSearchParams", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.deepEqual(
              JSON.parse(resp.responseText).form,
              { ok: "100" },
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST TypedArray", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um !== "vm") this.skip();
      const data = new Uint8Array([0x30, 0x31, 0x33]);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data,
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST TypedArray", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.strictEqual(
              JSON.parse(resp.responseText).data,
              "013",
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST ArrayBuffer", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um !== "vm") this.skip();
      const data = new Uint8Array([0x30, 0x31, 0x33]);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data: data.buffer,
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST ArrayBuffer", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.strictEqual(
              JSON.parse(resp.responseText).data,
              "013",
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("POST DataView", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um !== "vm") this.skip();
      const data = new Uint8Array([0x30, 0x31, 0x33]);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data: new DataView(data.buffer),
        method: "POST",
        onload(...args) {
          cbd("xhr onload POST DataView", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.strictEqual(
              JSON.parse(resp.responseText).data,
              "013",
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("PUT", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/put",
        data: "LemonMocha",
        method: "PUT",
        onload(...args) {
          cbd("xhr onload PUT", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.strictEqual(
              JSON.parse(resp.responseText).data,
              "LemonMocha",
              "上传内容不匹配",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("HEAD", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get",
        method: "HEAD",
        onload(...args) {
          cbd("xhr onload HEAD", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.include(
              resp.responseHeaders,
              "gunicorn/",
              "响应头内容不匹配",
            );
            switch (um) {
              case "tm":
                chai.assert.doesNotHaveAnyKeys(
                  resp,
                  ["response", "responseType", "responseText", "responseXML"],
                  "HEAD 请求只会返回响应头",
                );
                break;
              case "vm":
                chai.assert.strictEqual(
                  resp.response,
                  "",
                  "HEAD 请求响应内容应为空字符串",
                );
                chai.assert.strictEqual(
                  resp.responseText,
                  "",
                  "HEAD 请求响应内容应为空字符串",
                );
                chai.assert.strictEqual(
                  resp.responseXML,
                  "",
                  "HEAD 请求响应内容应为空字符串",
                );
                break;
              case "sc":
                chai.assert.doesNotHaveAnyKeys(
                  resp,
                  ["responseType", "responseText", "responseXML"],
                  "HEAD 请求只会返回响应头和空字符串的 response",
                );
                chai.assert.strictEqual(
                  resp.response,
                  "",
                  "HEAD 请求响应内容应为空字符串",
                );
                break;
            }
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("responseXML", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      // 脚本猫不支持
      // 暴力猴返回无实质内容
      if (um !== "tm") this.skip();
      GM_xmlhttpRequest({
        url: "https://httpbin.org/",
        onload(...args) {
          cbd("xhr onload responseXML", this, ...args);
          const resp = args[0];
          try {
            chai.assert.instanceOf(resp.responseXML, Document, "返回类型不符");
            chai.assert.strictEqual(
              resp.responseXML.title,
              "httpbin.org",
              "返回内容不符",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("重定向 finalUrl", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/redirect-to?url=https%3A%2F%2Fhttpbin.org%2F",
        onload(...args) {
          cbd("xhr onload finalUrl", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(
              resp.finalUrl,
              "https://httpbin.org/",
              "最终 URL 不匹配",
            );
            chai.assert.isOk(
              resp.responseText.startsWith("<!DOCTYPE html>"),
              "响应内容不正确",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("重定向 maxRedirects", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um !== "sc") this.skip();
      GM_xmlhttpRequest({
        url: "https://httpbin.org/redirect-to?url=https%3A%2F%2Fhttpbin.org%2F",
        maxRedirects: 0,
        onload(...args) {
          cbd("xhr onload maxRedirects", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(
              resp.finalUrl,
              "https://httpbin.org/redirect-to?url=https%3A%2F%2Fhttpbin.org%2F",
              "最终 URL 不匹配",
            );
            chai.assert.strictEqual(resp.status, 302, "重定向状态码不正确");
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
  });
  describe("响应类型", function () {
    it("text", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get?id=LemonMocha",
        responseType: "text",
        onload(...args) {
          cbd("xhr onload text", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.isString(resp.response, "text 类型应为 string");
            chai.assert.include(resp.response, "LemonMocha", "响应文本不匹配");
            if (um !== "vm") {
              chai.assert.strictEqual(
                resp.responseType,
                "text",
                "响应类型不匹配",
              );
            }
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("json", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get?id=LemonMocha",
        responseType: "json",
        onload(...args) {
          cbd("xhr onload json", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.isObject(resp.response, "json 类型应为对象或数组");
            chai.assert.strictEqual(
              resp.response.args.id,
              "LemonMocha",
              "解析内容不正确",
            );
            if (um !== "vm") {
              chai.assert.strictEqual(
                resp.responseType,
                "json",
                "响应类型不匹配",
              );
            }
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("blob", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get?id=LemonMocha",
        responseType: "blob",
        async onload(...args) {
          cbd("xhr onload blob", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.instanceOf(
              resp.response,
              Blob,
              "blob 类型应为 Blob 对象",
            );
            chai.assert.include(
              await resp.response.text(),
              "LemonMocha",
              "解析内容不正确",
            );
            if (um !== "vm") {
              chai.assert.strictEqual(
                resp.responseType,
                "blob",
                "响应类型不匹配",
              );
            }
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("document", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      // 暴力猴返回无实质内容
      if (um === "vm") this.skip();
      GM_xmlhttpRequest({
        url: "https://httpbin.org/",
        responseType: "document",
        onload(...args) {
          cbd("xhr onload document", this, ...args);
          const resp = args[0];
          try {
            chai.assert.instanceOf(resp.response, Document, "返回类型不符");
            chai.assert.strictEqual(
              resp.response.title,
              "httpbin.org",
              "返回内容不符",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("arraybuffer", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      GM_xmlhttpRequest({
        url: "https://httpbin.org/image/png",
        responseType: "arraybuffer",
        onload(...args) {
          cbd("xhr onload arraybuffer", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.instanceOf(
              resp.response,
              ArrayBuffer,
              "arraybuffer 类型应为 ArrayBuffer",
            );
            chai.assert.deepEqual(
              new Uint8Array(resp.response).slice(0, 4),
              new Uint8Array([0x89, 0x50, 0x4e, 0x47]),
              "解析内容不正确",
            );
            if (um !== "vm") {
              chai.assert.strictEqual(
                resp.responseType,
                "arraybuffer",
                "响应类型不匹配",
              );
            }
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("stream", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um === "vm") this.skip();
      GM_xmlhttpRequest({
        url: "https://httpbin.org/stream/40",
        responseType: "stream",
        onload(...args) {
          cbd("xhr onload stream", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "响应失败");
            chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            chai.assert.instanceOf(
              resp.response,
              ReadableStream,
              "stream 类型应为 ReadableStream",
            );
            chai.assert.strictEqual(
              resp.responseType,
              "stream",
              "响应类型不匹配",
            );
            let content = "";
            const reader = resp.response.getReader();
            function streamParser(stat) {
              if (stat.done) {
                done(
                  chai.assert.include(
                    content,
                    "https://httpbin.org/stream/40",
                    "内容测试",
                  ),
                );
              } else {
                content += String.fromCharCode.apply(null, stat.value);
                reader.read().then(streamParser);
              }
            }
            reader.read().then(streamParser);
          } catch (e) {
            done(e);
          }
        },
      });
    });
  });
  describe("事件", function () {
    it("onerror", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      GM_xmlhttpRequest({
        url: "https://httpbin.oox/get?id=LemonMocha",
        onerror(...args) {
          cbd("xhr onerror", this, ...args);
          const resp = args[0];
          try {
            if (um === "sc") {
              chai.assert.strictEqual(resp, "", "解析错误返回空字符串");
            } else {
              chai.assert.strictEqual(resp.status, 0, "响应错误应为 0");
              chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            }
            done();
          } catch (e) {
            done(e);
          }
        },
        onload(...args) {
          cbd("xhr onload onerror", this, ...args);
          try {
            chai.assert.fail("请求意外完成了");
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("onerror @connect", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um === "vm") this.skip();
      GM_xmlhttpRequest({
        url: "https://example.com/get?id=LemonMocha",
        onerror(...args) {
          cbd("xhr onerror @connect", this, ...args);
          const resp = args[0];
          try {
            if (um === "sc") {
              chai.assert.match(
                resp,
                /permission (not allowed|denied)/,
                "返回错误原因：选择忽略为 permission not allowed，选择拒绝为 permission denied",
              );
            } else {
              chai.assert.strictEqual(resp.status, 0, "响应错误应为 0");
              chai.assert.strictEqual(resp.readyState, 4, "请求状态不匹配");
            }
            done();
          } catch (e) {
            done(e);
          }
        },
        onload(...args) {
          cbd("xhr onload @connect", this, ...args);
          try {
            chai.assert.fail(
              um === "sc" ? "用户允许了此域名" : "请求意外完成了",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("onabort", function (done) {
      this.timeout(5000);
      // 暴力猴和脚本猫不触发 onabort 暂时跳过
      const um = document.querySelector("#um").value;
      if (um !== "tm") this.skip();
      const ret = GM_xmlhttpRequest({
        url: "https://httpbin.org/delay/4",
        onabort(...args) {
          cbd("xhr onabort", this, ...args);
          done();
        },
        onload(...args) {
          cbd("xhr onload onabort", this, ...args);
          try {
            chai.assert.fail("请求意外完成了");
            done();
          } catch (e) {
            done(e);
          }
        },
      });
      console.log("xhr return", typeof ret, ret);
      chai.assert.isObject(ret, "返回对象");
      chai.assert.property(ret, "abort", "返回对象包含 abort 方法");
      chai.assert.isFunction(ret.abort, "返回对象包含 abort 方法");
      setTimeout(ret.abort, 2000);
    });
    it("onloadstart", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get?id=LemonMocha",
        onloadstart(...args) {
          cbd("xhr onloadstart", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 0, "初始响应状态应为 0");
            chai.assert.strictEqual(resp.readyState, 1, "请求状态不匹配");
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("onprogress", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get?id=LemonMocha",
        onprogress(...args) {
          cbd("xhr onprogress", this, ...args);
          const resp = args[0];
          try {
            chai.assert.isTrue(
              resp.lengthComputable,
              "此请求响应长度为可计算的",
            );
            chai.assert.isAbove(resp.total, 0, "此请求响应长度应大于 0");
            chai.assert.isAtMost(
              resp.loaded,
              resp.total,
              "已下载长度不应大于总长度",
            );
          } catch (e) {
            done(e);
          }
        },
        onload(...args) {
          cbd("xhr onload onprogress", this, ...args);
          done();
        },
      });
    });
    it("onprogress stream", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um === "vm") this.skip();
      GM_xmlhttpRequest({
        url: "https://httpbin.org/stream/10",
        onprogress(...args) {
          cbd("xhr onprogress stream", this, ...args);
          const resp = args[0];
          try {
            chai.assert.isFalse(
              resp.lengthComputable,
              "此请求响应长度为不可计算的",
            );
            if (um === "sc") {
              chai.assert.strictEqual(
                resp.total,
                0,
                "不可计算的响应长度应为 0",
              );
            } else {
              chai.assert.strictEqual(
                resp.total,
                -1,
                "不可计算的响应长度应为 -1",
              );
            }
          } catch (e) {
            done(e);
          }
        },
        onload(...args) {
          cbd("xhr onload onprogress stream", this, ...args);
          done();
        },
      });
    });
    it("onprogress upload", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/post",
        data: "LemonMocha",
        method: "POST",
        upload: {
          onprogress(...args) {
            cbd("xhr upload.onprogress", this, ...args);
            const resp = args[0];
            try {
              chai.assert.isTrue(resp.lengthComputable, "上传长度为可计算的");
              chai.assert.strictEqual(resp.total, 10, "上传长度不匹配");
            } catch (e) {
              done(e);
            }
          },
        },
        onload(...args) {
          cbd("xhr onload upload.onprogress", this, ...args);
          done();
        },
      });
    });
    it("onreadystatechange", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      const callback = sinon.spy();
      sinon.assert.notCalled(callback);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get?id=LemonMocha",
        onreadystatechange: callback,
        onload(...args) {
          cbd("xhr onload onreadystatechange", this, ...args);
          callback.getCalls().forEach((call, i) => {
            cbd(
              "xhr onreadystatechange " + (i + 1),
              call.thisValue,
              ...call.args,
            );
          });
          try {
            switch (um) {
              // 4 次，1 2 3 4
              case "tm":
                sinon.assert.callCount(callback, 4);
                callback.getCalls().forEach((call, i) => {
                  chai.assert.strictEqual(
                    call.firstArg.readyState,
                    i + 1,
                    "请求状态不匹配",
                  );
                });
                break;
              // 2 次，2 4
              case "vm":
                sinon.assert.callCount(callback, 2);
                callback.getCalls().forEach((call, i) => {
                  chai.assert.strictEqual(
                    call.firstArg.readyState,
                    (i + 1) * 2,
                    "请求状态不匹配",
                  );
                });
                break;
              // 3 次，2 3 4
              case "sc":
                sinon.assert.callCount(callback, 3);
                callback.getCalls().forEach((call, i) => {
                  chai.assert.strictEqual(
                    call.firstArg.readyState,
                    i + 2,
                    "请求状态不匹配",
                  );
                });
                break;
            }
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("ontimeout", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/delay/4",
        timeout: 2000,
        ontimeout(...args) {
          cbd("xhr ontimeout", this, ...args);
          done();
        },
        onload(...args) {
          cbd("xhr onload ontimeout", this, ...args);
          try {
            chai.assert.fail("请求意外完成了");
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
  });
  describe("选项", function () {
    it("context", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um === "sc") this.skip();
      const time = Date.now() * Math.random();
      const contx = { start: time };
      GM_xmlhttpRequest({
        url: "https://httpbin.org/get",
        context: { ...contx },
        onload(...args) {
          cbd("xhr onload context", this, ...args);
          const resp = args[0];
          try {
            chai.assert.deepEqual(resp.context, contx, "上下文不匹配");
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("headers 添加", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/headers",
        headers: {
          Author: "LemonMocha",
        },
        onload(...args) {
          cbd("xhr onload headers 新增", this, ...args);
          const resp = args[0];
          try {
            const rtn = JSON.parse(resp.responseText);
            chai.assert.property(rtn.headers, "Author", "请求头没有被添加成功");
            chai.assert.strictEqual(
              rtn.headers["Author"],
              "LemonMocha",
              "请求头添加内容不符",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("headers 修改", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/headers",
        headers: {
          "user-agent": "LemonMocha",
        },
        onload(...args) {
          cbd("xhr onload headers 修改", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(
              JSON.parse(resp.responseText).headers["User-Agent"],
              "LemonMocha",
              "请求头修改内容不符",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("headers 删除", function (done) {
      this.timeout(5000);
      // 暴力猴似乎不支持 暂时跳过
      // 新版篡改猴无法删除
      const um = document.querySelector("#um").value;
      if (um !== "sc") this.skip();
      GM_xmlhttpRequest({
        url: "https://httpbin.org/headers",
        headers: {
          "user-agent": "",
        },
        onload(...args) {
          cbd("xhr onload headers 删除", this, ...args);
          const resp = args[0];
          try {
            chai.assert.notProperty(
              JSON.parse(resp.responseText).headers,
              "User-Agent",
              "请求头没有被删除",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("cookie", function (done) {
      this.timeout(5000);
      const um = document.querySelector("#um").value;
      if (um === "vm") this.skip();
      GM_xmlhttpRequest({
        url: "http://httpbin.org/cookies",
        cookie: "lemon=mocha;",
        onload(...args) {
          cbd("xhr onload cookie", this, ...args);
          const resp = args[0];
          try {
            const rtn = JSON.parse(resp.responseText);
            chai.assert.property(rtn.cookies, "lemon", "Cookie 没有被添加成功");
            chai.assert.strictEqual(
              rtn.cookies["lemon"],
              "mocha",
              "Cookie 内容不符",
            );
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
    it("基本验证", function (done) {
      this.timeout(5000);
      GM_xmlhttpRequest({
        url: "https://httpbin.org/basic-auth/lemon/mocha",
        user: "lemon",
        password: "mocha",
        onload(...args) {
          cbd("xhr onload 基本验证", this, ...args);
          const resp = args[0];
          try {
            chai.assert.strictEqual(resp.status, 200, "验证失败");
            done();
          } catch (e) {
            done(e);
          }
        },
      });
    });
  });
});
