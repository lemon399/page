describe("存储值 v4", function () {
  before("支持", function () {
    if (
      typeof GM != "object" ||
      typeof GM.getValue != "function" ||
      typeof GM.setValue != "function" ||
      typeof GM.listValues != "function" ||
      typeof GM.deleteValue != "function"
    ) {
      this.skip();
    }
  });
  beforeEach(async function () {
    for (let i = 1; i < 3; ++i) {
      await GM.deleteValue(`lemon_$test$_${i}`);
    }
  });
  it("未定义", async function () {
    const um = document.querySelector("#um").value;
    chai.assert.isUndefined(
      await GM.getValue("lemon_$test$_1"),
      "未定义应为 undefined",
    );
    chai.assert.strictEqual(
      await GM.getValue("lemon_$test$_1", 100),
      100,
      "未定义应返回默认值",
    );
    await GM.deleteValue("lemon_$test$_1");
    chai.assert.strictEqual(
      await GM.getValue("lemon_$test$_1", 100),
      100,
      "删除则未定义，应返回默认值",
    );
    await GM.setValue("lemon_$test$_1", undefined);
    // 只有篡改猴如此
    if (um === "tm") {
      chai.assert.isUndefined(
        await GM.getValue("lemon_$test$_1", 100),
        "赋值 undefined 并非未定义",
      );
    } else {
      chai.assert.strictEqual(
        await GM.getValue("lemon_$test$_1", 100),
        100,
        "赋值 undefined 等同于删除，应返回默认值",
      );
    }
  });
  it("GM.setValue / GM.getValue", async function () {
    const um = document.querySelector("#um").value;
    const stat = await GM.setValue("lemon_$test$_1", 10);
    switch (um) {
      case "tm":
        chai.assert.isUndefined(stat, "应 resolve undefined");
        chai.assert.typeOf(
          GM.getValue("lemon_$test$_1").then,
          "function",
          "应为 Promise",
        );
        break;
      case "vm":
        chai.assert.isNull(stat, "应 resolve null");
        chai.assert.instanceOf(
          GM.getValue("lemon_$test$_1"),
          Promise,
          "GM.getValue 是异步函数",
        );
        break;
      case "sc":
        chai.assert.isBoolean(stat, "会 resolve 是否赋值成功");
        chai.assert.notInstanceOf(
          GM.getValue("lemon_$test$_1"),
          Promise,
          "GM.getValue 是同步函数",
        );
        break;
    }
    chai.assert.strictEqual(
      await GM.getValue("lemon_$test$_1"),
      10,
      "未返回设置的值",
    );
    chai.assert.notStrictEqual(
      await GM.getValue("lemon_$test$_1", 100),
      100,
      "已设置的值不应返回默认值",
    );
  });
  it("GM.listValues / GM.deleteValue", async function () {
    const um = document.querySelector("#um").value;
    chai.assert.isArray(await GM.listValues(), "GM.listValues resolve 数组");
    for (let i = 1; i < 3; ++i) {
      chai.assert.notInclude(
        await GM.listValues(),
        `lemon_$test$_${i}`,
        "初始化不通过",
      );
    }
    await GM.setValue("lemon_$test$_1", 1);
    chai.assert.include(
      await GM.listValues(),
      "lemon_$test$_1",
      "应包含定义的 key",
    );
    await GM.setValue("lemon_$test$_2", 1);
    chai.assert.include(
      await GM.listValues(),
      "lemon_$test$_1",
      "应包含定义的 key",
    );
    chai.assert.include(
      await GM.listValues(),
      "lemon_$test$_2",
      "应包含定义的 key",
    );
    await GM.deleteValue("lemon_$test$_1");
    chai.assert.notInclude(
      await GM.listValues(),
      "lemon_$test$_1",
      "不应包含未定义的 key",
    );
    chai.assert.include(
      await GM.listValues(),
      "lemon_$test$_2",
      "应包含定义的 key",
    );
    // 篡改猴：不删除值，不返回默认值
    // 暴力猴：不删除值，返回默认值
    // 脚本猫：删除值，返回默认值
    if (um === "sc") {
      await GM.setValue("lemon_$test$_2", undefined);
      chai.assert.notInclude(
        await GM.listValues(),
        "lemon_$test$_2",
        "不应包含未定义的 key",
      );
    }
    await GM.deleteValue("lemon_$test$_2");
    chai.assert.notInclude(
      await GM.listValues(),
      "lemon_$test$_2",
      "不应包含未定义的 key",
    );
  });
  describe("存储不同类型", function () {
    beforeEach(async function () {
      await GM.deleteValue("lemon_$test$_1");
    });
    it("undefined", async function () {
      await GM.setValue("lemon_$test$_1", void 0);
      chai.assert.isUndefined(
        await GM.getValue("lemon_$test$_1"),
        "应为 undefined",
      );
    });
    it("null", async function () {
      await GM.setValue("lemon_$test$_1", null);
      chai.assert.isNull(await GM.getValue("lemon_$test$_1"), "应为 null");
    });
    it("boolean", async function () {
      await GM.setValue("lemon_$test$_1", true);
      chai.assert.strictEqual(
        await GM.getValue("lemon_$test$_1"),
        true,
        "应为 true",
      );
    });
    it("string", async function () {
      await GM.setValue("lemon_$test$_1", "ok");
      chai.assert.strictEqual(
        await GM.getValue("lemon_$test$_1"),
        "ok",
        '应为 "ok"',
      );
    });
    it("number", async function () {
      await GM.setValue("lemon_$test$_1", 123);
      chai.assert.strictEqual(
        await GM.getValue("lemon_$test$_1"),
        123,
        "应为 123",
      );
    });
    it("NaN", async function () {
      const um = document.querySelector("#um").value;
      await GM.setValue("lemon_$test$_1", NaN);
      if (um === "sc") {
        chai.assert.isUndefined(
          await GM.getValue("lemon_$test$_1"),
          "应为 undefined",
        );
      } else {
        chai.assert.isNaN(await GM.getValue("lemon_$test$_1"), "应为 NaN");
      }
    });
    it("Infinity", async function () {
      await GM.setValue("lemon_$test$_1", Infinity);
      chai.assert.strictEqual(
        await GM.getValue("lemon_$test$_1"),
        Infinity,
        "应为 Infinity",
      );
    });
    it("-Infinity", async function () {
      await GM.setValue("lemon_$test$_1", -Infinity);
      chai.assert.strictEqual(
        await GM.getValue("lemon_$test$_1"),
        -Infinity,
        "应为 -Infinity",
      );
    });
    it("new Number", async function () {
      const um = document.querySelector("#um").value;
      await GM.setValue("lemon_$test$_1", new Number(10));
      if (um === "sc") {
        chai.assert.strictEqual(
          await GM.getValue("lemon_$test$_1"),
          10,
          "会返回 10",
        );
      } else {
        chai.assert.deepEqual(
          await GM.getValue("lemon_$test$_1"),
          {},
          "会返回 {}",
        );
      }
    });
    it("bigint", async function () {
      const um = document.querySelector("#um").value;
      // 永不停止的 Promise
      if (um === "sc") this.skip();
      switch (um) {
        case "tm":
          await GM.setValue("lemon_$test$_1", 12345678987654321n);
          chai.assert.isUndefined(
            await GM.getValue("lemon_$test$_1"),
            "会返回 undefined",
          );
          break;
        case "vm":
          chai.assert.throws(
            GM.setValue.bind(this, "lemon_$test$_1", 12345678987654321n),
            "Do not know how to serialize a BigInt",
            "会抛错误",
          );
          break;
      }
    });
    it("function", async function () {
      const um = document.querySelector("#um").value;
      const fn = () => 0;
      // 永不停止的 Promise
      if (um === "sc") this.skip();
      await GM.setValue("lemon_$test$_1", fn);
      switch (um) {
        case "tm":
          chai.assert.isUndefined(
            await GM.getValue("lemon_$test$_1"),
            "应返回 undefined",
          );
          break;
        case "vm":
          chai.assert.strictEqual(
            await GM.getValue("lemon_$test$_1"),
            "undefined",
            "应为字符串 'undefined'",
          );
          break;
      }
    });
    it("Object", async function () {
      await GM.setValue("lemon_$test$_1", { a: false, b: 99, c: { d: null } });
      chai.assert.deepEqual(
        await GM.getValue("lemon_$test$_1"),
        { a: false, b: 99, c: { d: null } },
        "应返回一致的对象",
      );
    });
    it("Map", async function () {
      await GM.setValue(
        "lemon_$test$_1",
        new Map([
          [1, true],
          [2, "ok"],
          ["null", void 0],
          [{ a: 3 }, 0],
        ]),
      );
      chai.assert.deepEqual(
        await GM.getValue("lemon_$test$_1"),
        {},
        "会返回 {}",
      );
    });
    it("Array", async function () {
      await GM.setValue("lemon_$test$_1", [
        30,
        void 0,
        false,
        "ok",
        [{}, null],
      ]);
      chai.assert.deepEqual(
        await GM.getValue("lemon_$test$_1"),
        [30, null, false, "ok", [{}, null]],
        "会返回一致的数组，但 undefined 值会变为 null",
      );
    });
    it("TypedArray", async function () {
      await GM.setValue(
        "lemon_$test$_1",
        new Uint8Array([0xc0, 0xff, 0xee, 0x5]),
      );
      chai.assert.deepEqual(
        await GM.getValue("lemon_$test$_1"),
        { 0: 192, 1: 255, 2: 238, 3: 5 },
        "会返回类数组、无 length 的对象",
      );
    });
    it("Set", async function () {
      await GM.setValue(
        "lemon_$test$_1",
        new Set([30, void 0, false, "ok", [{}, null]]),
      );
      chai.assert.deepEqual(
        await GM.getValue("lemon_$test$_1"),
        {},
        "会返回 {}",
      );
    });
    it("Symbol", async function () {
      const um = document.querySelector("#um").value;
      const sym = Symbol("ok");
      // 永不停止的 Promise
      if (um === "sc") this.skip();
      await GM.setValue("lemon_$test$_1", sym);
      switch (um) {
        case "tm":
          chai.assert.isUndefined(
            await GM.getValue("lemon_$test$_1"),
            "应返回 undefined",
          );
          break;
        case "vm":
          chai.assert.strictEqual(
            await GM.getValue("lemon_$test$_1"),
            "undefined",
            "应为字符串 'undefined'",
          );
          break;
      }
    });
    it("RegExp", async function () {
      await GM.setValue("lemon_$test$_1", /^ok?.$/);
      chai.assert.deepEqual(
        await GM.getValue("lemon_$test$_1"),
        {},
        "会返回 {}",
      );
    });
  });
});

describe("存储值监听 v4", function () {
  before("支持", function () {
    if (
      typeof GM != "object" ||
      typeof GM.deleteValue != "function" ||
      typeof GM.setValue != "function" ||
      typeof GM.addValueChangeListener != "function" ||
      typeof GM.removeValueChangeListener != "function"
    ) {
      this.skip();
    }
  });
  beforeEach(async function () {
    await GM.deleteValue("lemon_$test$_1");
    await GM.setValue("lemon_$test$_2", 10);
  });
  it("监听器 ID", async function () {
    const um = document.querySelector("#um").value;
    const id = await GM.addValueChangeListener("lemon_$test$_1", () => 0);
    await GM.removeValueChangeListener(id);
    if (um === "vm") {
      chai.assert.isString(id, "监听器 ID 应为字符串");
    } else {
      chai.assert.isNumber(id, "监听器 ID 应为数字");
    }
  });
  it("修改值", async function () {
    const um = document.querySelector("#um").value;
    const callback = sinon.spy();
    sinon.assert.notCalled(callback);
    const id = await GM.addValueChangeListener("lemon_$test$_2", callback);
    sinon.assert.notCalled(callback);
    await GM.setValue("lemon_$test$_2", true);
    callback.getCalls().forEach((call, i) => {
      cbd("vcl 修改值 " + (i + 1), call.thisValue, ...call.args);
    });
    sinon.assert.calledOnce(callback);
    sinon.assert.calledWith(callback, "lemon_$test$_2", 10, true, false);
    await GM.removeValueChangeListener(id);
    await GM.setValue("lemon_$test$_2", 10);
    sinon.assert.calledOnce(callback);
    if (um === "tm") {
      chai.assert.hasAllKeys(
        callback.firstCall.thisValue,
        ["id", "key", "cb"],
        "监听回调的 this 为 { id, key, cb }",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.id,
        id,
        "this.id 应为监听器 ID",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.key,
        "lemon_$test$_2",
        "this.key 应为被监听值的 key",
      );
      chai.assert.isFunction(
        callback.firstCall.thisValue.cb,
        "this.cb 应为回调函数",
      );
    }
  });
  it("新增值", async function () {
    const um = document.querySelector("#um").value;
    const callback = sinon.spy();
    sinon.assert.notCalled(callback);
    const id = await GM.addValueChangeListener("lemon_$test$_1", callback);
    sinon.assert.notCalled(callback);
    await GM.setValue("lemon_$test$_1", "ok");
    callback.getCalls().forEach((call, i) => {
      cbd("vcl 新增值 " + (i + 1), call.thisValue, ...call.args);
    });
    sinon.assert.calledOnce(callback);
    sinon.assert.calledWith(callback, "lemon_$test$_1", undefined, "ok", false);
    await GM.removeValueChangeListener(id);
    if (um === "tm") {
      chai.assert.hasAllKeys(
        callback.firstCall.thisValue,
        ["id", "key", "cb"],
        "监听回调的 this 为 { id, key, cb }",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.id,
        id,
        "this.id 应为监听器 ID",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.key,
        "lemon_$test$_1",
        "this.key 应为被监听值的 key",
      );
      chai.assert.isFunction(
        callback.firstCall.thisValue.cb,
        "this.cb 应为回调函数",
      );
    }
  });
  it("删除值", async function () {
    const um = document.querySelector("#um").value;
    // 脚本猫触发变更回调过晚 暂时跳过
    if (um === "sc") this.skip();
    const callback = sinon.spy();
    sinon.assert.notCalled(callback);
    const id = await GM.addValueChangeListener("lemon_$test$_2", callback);
    sinon.assert.notCalled(callback);
    await GM.deleteValue("lemon_$test$_2");
    callback.getCalls().forEach((call, i) => {
      cbd("vcl 删除值 " + (i + 1), call.thisValue, ...call.args);
    });
    sinon.assert.calledOnce(callback);
    sinon.assert.calledWith(callback, "lemon_$test$_2", 10, undefined, false);
    await GM.removeValueChangeListener(id);
    if (um === "tm") {
      chai.assert.hasAllKeys(
        callback.firstCall.thisValue,
        ["id", "key", "cb"],
        "监听回调的 this 为 { id, key, cb }",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.id,
        id,
        "this.id 应为监听器 ID",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.key,
        "lemon_$test$_2",
        "this.key 应为被监听值的 key",
      );
      chai.assert.isFunction(
        callback.firstCall.thisValue.cb,
        "this.cb 应为回调函数",
      );
    }
  });
});
