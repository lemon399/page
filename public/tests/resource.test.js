describe("脚本资源", function () {
  describe("文本内容", function () {
    before("支持", function () {
      if (typeof GM_getResourceText != "function") {
        this.skip();
      }
    });
    it("常规", function () {
      chai.assert.include(
        GM_getResourceText("ts"),
        ".hitokoto",
        "应返回资源 ts 的内容",
      );
    });
    it("正确 SRI", function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoSRI"] || um !== "tm") {
        this.skip();
      } else {
        chai.assert.include(
          GM_getResourceText("rts"),
          "MonkeyExposerNoSRI",
          "应返回资源 ts 的内容",
        );
      }
    });
    it("错误 SRI", function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoSRI"] || um !== "tm") {
        this.skip();
      } else {
        chai.assert.isEmpty(GM_getResourceText("rte"), "应返回空字符串");
      }
    });
    it("资源不存在", function () {
      const um = document.querySelector("#um").value;
      if (um === "tm") {
        chai.assert.isNull(GM_getResourceText("nts"), "应返回 null");
      } else {
        chai.assert.isUndefined(GM_getResourceText("nts"), "应返回 undefined");
      }
    });
    it("data:", function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoData"] || um === "vm") this.skip();
      chai.assert.include(
        GM_getResourceText("rd"),
        "reqData",
        "应返回资源 ts 的内容",
      );
    });
    it("data: base64", function () {
      if (window["MonkeyExposerNoData"]) this.skip();
      chai.assert.include(
        GM_getResourceText("rb"),
        "reqDataBase64",
        "应返回资源 ts 的内容",
      );
    });
  });
  describe("URL", function () {
    before("支持", function () {
      if (typeof GM_getResourceURL != "function") {
        this.skip();
      }
    });
    it("常规", function () {
      chai.assert.match(
        GM_getResourceURL("ts"),
        /^(data|blob):.+/,
        "应返回 data: 或 blob: 地址",
      );
    });
    it("blob:", function () {
      const um = document.querySelector("#um").value;
      if (um === "tm") this.skip();
      chai.assert.match(
        GM_getResourceURL("ts", true),
        /^blob:.+/,
        "应返回 blob: 地址",
      );
    });
    it("data:", function () {
      const um = document.querySelector("#um").value;
      if (um === "tm") this.skip();
      chai.assert.match(
        GM_getResourceURL("ts", false),
        /^data:.+/,
        "应返回 data: 地址",
      );
    });
    it("正确 SRI", function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoSRI"] || um !== "tm") {
        this.skip();
      } else {
        chai.assert.match(
          GM_getResourceURL("rts"),
          /^(data|blob):.+/,
          "应返回 data: 或 blob: 地址",
        );
      }
    });
    it("错误 SRI", function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoSRI"] || um !== "tm") {
        this.skip();
      } else {
        chai.assert.isEmpty(GM_getResourceURL("rte"), "应返回空字符串");
      }
    });
    it("资源不存在", function () {
      const um = document.querySelector("#um").value;
      if (um === "tm") {
        chai.assert.isNull(GM_getResourceURL("nts"), "应返回 null");
      } else {
        chai.assert.isUndefined(GM_getResourceURL("nts"), "应返回 undefined");
      }
    });
  });
});
