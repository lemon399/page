// ==UserScript==
// @name         Get API of Monkey for Mocha
// @version      1.1.5
// @description  Get API of Monkey to scan it.
// @namespace    https://lemon399-bitbucket-io.vercel.app/
// @author       lemon399
// @copyright    MIT
// @match        https://lemon399.gitlab.io/page/tests/*
// @run-at       document-start
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_listValues
// @grant        GM_deleteValue
// @grant        GM_getResourceURL
// @grant        GM_getResourceText
// @grant        GM_registerMenuCommand
// @grant        GM_unregisterMenuCommand
// @grant        GM_addStyle
// @grant        GM_addElement
// @grant        GM_addValueChangeListener
// @grant        GM_removeValueChangeListener
// @grant        GM_openInTab
// @grant        GM_getTab
// @grant        GM_getTabs
// @grant        GM_saveTab
// @grant        GM_xmlhttpRequest
// @grant        GM_download
// @grant        GM_notification
// @grant        GM_setClipboard
// @grant        GM_log
// @grant        unsafeWindow
// @grant        GM.setValue
// @grant        GM.getValue
// @grant        GM.listValues
// @grant        GM.deleteValue
// @grant        GM.addValueChangeListener
// @grant        GM.removeValueChangeListener
// @grant        GM.getResourceUrl
// @grant        GM.getResourceURL
// @grant        GM.getResourceText
// @grant        GM.registerMenuCommand
// @grant        GM.unregisterMenuCommand
// @grant        GM.notification
// @grant        GM.setClipboard
// @grant        GM.xmlHttpRequest
// @grant        GM.download
// @grant        GM.openInTab
// @grant        GM.getTab
// @grant        GM.getTabs
// @grant        GM.saveTab
// @grant        GM.addStyle
// @grant        GM.addElement
// @require      https://lemon399.gitlab.io/page/tests/require-nosri.user.js
// @require      https://lemon399.gitlab.io/page/tests/require-rightsri.user.js#sha256-OYyyxWZMKer+4joBR4Q8+BKGBSYpD5lVwc8bKPzjiaM=
// @require      https://lemon399.gitlab.io/page/tests/require-errorsri.user.js#md5=65365265635656345761286046295536
// @require      https://lemon399.gitlab.io/page/tests/require-haslog.user.js
// @require      data:text/javascript,const%20reqData%20%3D%20true%3B
// @require      data:text/javascript;base64,Y29uc3QgcmVxRGF0YUJhc2U2NCA9IHRydWU7
// @resource     ts https://v1.hitokoto.cn/?encode=js
// @resource     rts https://lemon399.gitlab.io/page/tests/require.test.js#md5=84a3f44c3e54bbe8fd776f05f255dada
// @resource     rte https://lemon399.gitlab.io/page/tests/require.test.js#md5=187ad06000006ebc1b821a942db149a8
// @resource     rd data:text/javascript,const%20reqData%20%3D%20true%3B
// @resource     rb data:text/javascript;base64,Y29uc3QgcmVxRGF0YUJhc2U2NCA9IHRydWU7
// @connect      httpbin.org
// @connect      httpbin.oox
// @inject-into  page
// @name:zh-CN   油猴 API 测试
// @description:zh-CN 暴露 API 以便测试和调试
// @noframes
// @updateURL  https://lemon399.gitlab.io/page/tests/expose.user.js
// @downloadURL  https://lemon399.gitlab.io/page/tests/expose.user.js
// @supportURL   https://gitlab.com/lemon399/page
// @homepageURL  https://lemon399.gitlab.io/page/tests/mocha.html
// @antifeature  tracking Just a test
// @antifeature:zh-CN  tracking 你猜有没有跟踪:)
// @sandbox      JavaScript
// @storageName  LemonMocha
// @icon         https://lemon399.gitlab.io/page/favicon.ico
// ==/UserScript==

/* global GMRequiredJSLoaded, GM, unsafeWindow, GM_info, GM_registerMenuCommand, GM_unregisterMenuCommand, GM_getValue, GM_deleteValue, GM_setValue, GM_listValues, GM_addStyle, GM_addElement, GM_xmlhttpRequest, GM_download, GM_getResourceURL, GM_getResourceText, GM_addValueChangeListener, GM_removeValueChangeListener, GM_openInTab, GM_getTab, GM_getTabs, GM_saveTab, GM_notification, GM_setClipboard, GM_log */
/* eslint-disable no-global-assign */

(function () {
  "use strict";

  try {
    if (typeof unsafeWindow !== "object") unsafeWindow = window;
    unsafeWindow.MonkeyExposerVer = 10105;
    if (
      typeof GM === "object" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM")
    )
      unsafeWindow.GM = GM;
    if (
      typeof GM_info !== "undefined" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_info")
    )
      unsafeWindow.GM_info = GM_info;
    if (
      typeof GM_setValue === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_setValue")
    )
      unsafeWindow.GM_setValue = GM_setValue;
    if (
      typeof GM_getValue === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_getValue")
    )
      unsafeWindow.GM_getValue = GM_getValue;
    if (
      typeof GM_listValues === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_listValues")
    )
      unsafeWindow.GM_listValues = GM_listValues;
    if (
      typeof GM_deleteValue === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_deleteValue")
    )
      unsafeWindow.GM_deleteValue = GM_deleteValue;
    if (
      typeof GM_getResourceURL === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_getResourceURL")
    )
      unsafeWindow.GM_getResourceURL = GM_getResourceURL;
    if (
      typeof GM_getResourceText === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_getResourceText")
    )
      unsafeWindow.GM_getResourceText = GM_getResourceText;
    if (
      typeof GM_registerMenuCommand === "function" &&
      !Object.prototype.hasOwnProperty.call(
        unsafeWindow,
        "GM_registerMenuCommand",
      )
    )
      unsafeWindow.GM_registerMenuCommand = GM_registerMenuCommand;
    if (
      typeof GM_unregisterMenuCommand === "function" &&
      !Object.prototype.hasOwnProperty.call(
        unsafeWindow,
        "GM_unregisterMenuCommand",
      )
    )
      unsafeWindow.GM_unregisterMenuCommand = GM_unregisterMenuCommand;
    if (
      typeof GM_addStyle === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_addStyle")
    )
      unsafeWindow.GM_addStyle = GM_addStyle;
    if (
      typeof GM_addElement === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_addElement")
    )
      unsafeWindow.GM_addElement = GM_addElement;
    if (
      typeof GM_addValueChangeListener === "function" &&
      !Object.prototype.hasOwnProperty.call(
        unsafeWindow,
        "GM_addValueChangeListener",
      )
    )
      unsafeWindow.GM_addValueChangeListener = GM_addValueChangeListener;
    if (
      typeof GM_removeValueChangeListener === "function" &&
      !Object.prototype.hasOwnProperty.call(
        unsafeWindow,
        "GM_removeValueChangeListener",
      )
    )
      unsafeWindow.GM_removeValueChangeListener = GM_removeValueChangeListener;
    if (
      typeof GM_openInTab === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_openInTab")
    )
      unsafeWindow.GM_openInTab = GM_openInTab;
    if (
      typeof GM_getTab === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_getTab")
    )
      unsafeWindow.GM_getTab = GM_getTab;
    if (
      typeof GM_getTabs === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_getTabs")
    )
      unsafeWindow.GM_getTabs = GM_getTabs;
    if (
      typeof GM_saveTab === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_saveTab")
    )
      unsafeWindow.GM_saveTab = GM_saveTab;
    if (
      typeof GM_xmlhttpRequest === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_xmlhttpRequest")
    )
      unsafeWindow.GM_xmlhttpRequest = GM_xmlhttpRequest;
    if (
      typeof GM_download === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_download")
    )
      unsafeWindow.GM_download = GM_download;
    if (
      typeof GM_notification === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_notification")
    )
      unsafeWindow.GM_notification = GM_notification;
    if (
      typeof GM_setClipboard === "function" &&
      !Object.prototype.hasOwnProperty.call(unsafeWindow, "GM_setClipboard")
    )
      unsafeWindow.GM_setClipboard = GM_setClipboard;
    unsafeWindow.reqNoSRI = typeof reqNoSRI;
    unsafeWindow.reqHasLog = typeof reqHasLog === "boolean" ? reqHasLog : false;
    unsafeWindow.reqErrorSRI = typeof reqErrorSRI;
    unsafeWindow.reqRightSRI = typeof reqRightSRI;
    unsafeWindow.reqData = typeof reqData;
    unsafeWindow.reqDataBase64 = typeof reqDataBase64;
    unsafeWindow.safeWindow = window;
  } catch (error) {
    alert(error.message);
  }
})();
