describe("@require", function () {
  it("无 SRI", function () {
    chai.assert.strictEqual(
      window["reqNoSRI"],
      "boolean",
      "@require 没有导入依赖脚本",
    );
  });
  it("正确 SRI", function () {
    const um = document.querySelector("#um").value;
    if (window["MonkeyExposerNoSRI"] || um === "vm") {
      this.skip();
    } else {
      chai.assert.strictEqual(
        window["reqRightSRI"],
        "boolean",
        "@require 没有正确导入依赖脚本",
      );
    }
  });
  it("错误 SRI", function () {
    const um = document.querySelector("#um").value;
    if (window["MonkeyExposerNoSRI"] || um === "vm") {
      this.skip();
    } else {
      chai.assert.strictEqual(
        window["reqErrorSRI"],
        "undefined",
        "@require 不应导入校验错误的依赖脚本",
      );
    }
  });
  it("包含 API", function () {
    chai.assert.isOk(window["reqHasLog"], "@require 没有给依赖脚本分配 API");
  });
  it("data:", function () {
    if (window["MonkeyExposerNoData"]) this.skip();
    chai.assert.strictEqual(
      window["reqData"],
      "boolean",
      "@require 没有导入 data: 依赖脚本",
    );
  });
  it("data: Base64", function () {
    if (window["MonkeyExposerNoData"]) this.skip();
    chai.assert.strictEqual(
      window["reqDataBase64"],
      "boolean",
      "@require 没有导入 Base64 编码的 data: 依赖脚本",
    );
  });
});
