describe("添加样式和元素", function () {
  describe("GM_addStyle", function () {
    before("支持", function () {
      if (typeof GM_addStyle != "function") {
        this.skip();
      }
    });
    it("GM_addStyle", function () {
      const elem = GM_addStyle("#inst { color: tan; }");
      chai.assert.strictEqual(
        getComputedStyle(document.getElementById("inst")).color,
        "rgb(210, 180, 140)",
        "没有成功应用样式",
      );
      chai.assert.instanceOf(elem, HTMLStyleElement, "返回注入的元素");
      chai.assert.strictEqual(
        elem.parentNode.tagName,
        "HEAD",
        "在 <head> 注入 <style>",
      );
    });
    it("GM_addStyle callback", function () {
      const um = document.querySelector("#um").value;
      if (um !== "tm") this.skip();
      const callback = sinon.spy();
      sinon.assert.notCalled(callback);
      const elem = GM_addStyle("#inst { color: tan; }", callback);
      sinon.assert.calledOnce(callback);
      chai.assert.strictEqual(
        getComputedStyle(document.getElementById("inst")).color,
        "rgb(210, 180, 140)",
        "没有成功应用样式",
      );
      chai.assert.instanceOf(elem, HTMLStyleElement, "返回注入的元素");
      chai.assert.strictEqual(
        elem.parentNode.tagName,
        "HEAD",
        "在 <head> 注入 <style>",
      );
    });
    it("GM_addStyle.then", function (done) {
      const um = document.querySelector("#um").value;
      if (um !== "vm") this.skip();
      const elem = GM_addStyle("#inst { color: purple; }");
      chai.assert.notInstanceOf(elem, Promise, "不应为 Promise");
      chai.assert.isFunction(elem.then, "带有 then 函数");
      elem.then(function (el) {
        chai.assert.strictEqual(
          getComputedStyle(document.getElementById("inst")).color,
          "rgb(128, 0, 128)",
          "没有成功应用样式",
        );
        chai.assert.instanceOf(el, HTMLStyleElement, "回调参数为注入的元素");
        chai.assert.strictEqual(
          el.parentNode.tagName,
          "HEAD",
          "在 <head> 注入 <style>",
        );
        done();
      });
    });
  });
  describe("GM_addElement", function () {
    before("支持", function () {
      if (typeof GM_addElement != "function") {
        this.skip();
      }
    });
    it("GM_addElement(t, a)", function () {
      const um = document.querySelector("#um").value;
      const elem = GM_addElement("span", {
        textContent: "OK",
        style: "color: snow;",
        test: "OK",
      });
      chai.assert.instanceOf(elem, HTMLSpanElement, "返回注入的元素");
      chai.assert.strictEqual(elem.innerText, "OK", "未设置内容");
      chai.assert.strictEqual(elem.style.cssText, "color: snow;", "未设置属性");
      chai.assert.strictEqual(
        elem.getAttribute("test"),
        "OK",
        "应为 HTML 属性，除了 textContent",
      );
      chai.assert.isUndefined(elem.test, "不应为 DOM 属性，除了 textContent");
      if (um === "vm") {
        chai.assert.strictEqual(
          elem.parentNode.tagName,
          "BODY",
          "默认在 <body> 注入元素",
        );
      } else {
        chai.assert.strictEqual(
          elem.parentNode.tagName,
          "HEAD",
          "默认在 <head> 注入元素",
        );
      }
    });
    it("GM_addElement(t, a, c)", function () {
      const um = document.querySelector("#um").value;
      if (um !== "tm") this.skip();
      const callback = sinon.spy();
      sinon.assert.notCalled(callback);
      const elem = GM_addElement(
        "span",
        {
          textContent: "OK",
          style: "color: snow;",
        },
        callback,
      );
      sinon.assert.calledOnce(callback);
      chai.assert.instanceOf(elem, HTMLSpanElement, "返回注入的元素");
      chai.assert.strictEqual(elem.innerText, "OK", "未设置内容");
      chai.assert.strictEqual(elem.style.cssText, "color: snow;", "未设置属性");
      chai.assert.strictEqual(
        elem.parentNode.tagName,
        "HEAD",
        "默认在 <head> 注入元素",
      );
    });
    it("GM_addElement(p, t, a)", function () {
      const elem = GM_addElement(document.getElementById("inst"), "span", {
        textContent: "OK",
        style: "color: snow;",
      });
      chai.assert.instanceOf(elem, HTMLSpanElement, "返回注入的元素");
      chai.assert.strictEqual(elem.innerText, "OK", "未设置内容");
      chai.assert.strictEqual(elem.style.cssText, "color: snow;", "未设置属性");
      chai.assert.strictEqual(elem.parentNode.id, "inst", "父元素不正确");
    });
    it("GM_addElement(p, t, a, c)", function () {
      const um = document.querySelector("#um").value;
      if (um !== "tm") this.skip();
      const callback = sinon.spy();
      sinon.assert.notCalled(callback);
      const elem = GM_addElement(
        document.getElementById("inst"),
        "span",
        {
          textContent: "OK",
          style: "color: snow;",
        },
        callback,
      );
      sinon.assert.calledOnce(callback);
      chai.assert.instanceOf(elem, HTMLSpanElement, "返回注入的元素");
      chai.assert.strictEqual(elem.innerText, "OK", "未设置内容");
      chai.assert.strictEqual(elem.style.cssText, "color: snow;", "未设置属性");
      chai.assert.strictEqual(elem.parentNode.id, "inst", "父元素不正确");
    });
    it("GM_addElement(t, a).then", function (done) {
      const um = document.querySelector("#um").value;
      if (um !== "vm") this.skip();
      const elem = GM_addElement("span", {
        textContent: "OK",
        style: "color: snow;",
      });
      chai.assert.notInstanceOf(elem, Promise, "不应为 Promise");
      chai.assert.isFunction(elem.then, "带有 then 函数");
      elem.then(function (el) {
        chai.assert.instanceOf(el, HTMLSpanElement, "返回注入的元素");
        chai.assert.strictEqual(el.innerText, "OK", "未设置内容");
        chai.assert.strictEqual(el.style.cssText, "color: snow;", "未设置属性");
        chai.assert.strictEqual(
          el.parentNode.tagName,
          "BODY",
          "默认在 <body> 注入元素",
        );
        done();
      });
    });
    it("GM_addElement(p, t, a).then", function () {
      const um = document.querySelector("#um").value;
      if (um !== "vm") this.skip();
      const elem = GM_addElement(document.getElementById("inst"), "span", {
        textContent: "OK",
        style: "color: snow;",
      });
      chai.assert.notInstanceOf(elem, Promise, "不应为 Promise");
      chai.assert.isFunction(elem.then, "带有 then 函数");
      elem.then((el) => {
        chai.assert.instanceOf(el, HTMLSpanElement, "返回注入的元素");
        chai.assert.strictEqual(el.innerText, "OK", "未设置内容");
        chai.assert.strictEqual(el.style.cssText, "color: snow;", "未设置属性");
        chai.assert.strictEqual(el.parentNode.id, "inst", "父元素不正确");
      });
    });
  });
});
