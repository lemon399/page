async function instantiate(module, imports = {}) {
  const adaptedImports = {
    env: Object.assign(Object.create(globalThis), imports.env || {}, {
      abort(message, fileName, lineNumber, columnNumber) {
        // ~lib/builtins/abort(~lib/string/String | null?, ~lib/string/String | null?, u32?, u32?) => void
        message = __liftString(message >>> 0);
        fileName = __liftString(fileName >>> 0);
        lineNumber = lineNumber >>> 0;
        columnNumber = columnNumber >>> 0;
        (() => {
          // @external.js
          throw Error(`${message} in ${fileName}:${lineNumber}:${columnNumber}`);
        })();
      },
      "asDom.setDataStrOfNode"(node, key, value) {
        // assembly/index/setDataStrOfNode(i32, ~lib/string/String, ~lib/string/String) => void
        key = __liftString(key >>> 0);
        value = __liftString(value >>> 0);
        asDom.setDataStrOfNode(node, key, value);
      },
      "asDom.setDataIntOfNode"(node, key, value) {
        // assembly/index/setDataIntOfNode(i32, ~lib/string/String, i32) => void
        key = __liftString(key >>> 0);
        asDom.setDataIntOfNode(node, key, value);
      },
      "asDom.setPropStrOfNode"(node, key, value) {
        // assembly/index/setPropStrOfNode(i32, ~lib/string/String, ~lib/string/String) => void
        key = __liftString(key >>> 0);
        value = __liftString(value >>> 0);
        asDom.setPropStrOfNode(node, key, value);
      },
      "asDom.clearTimeout"(id) {
        // assembly/index/clearTimeout(f64) => void
        asDom.clearTimeout(id);
      },
      seed() {
        // ~lib/builtins/seed() => f64
        return (() => {
          // @external.js
          return Date.now() * Math.random();
        })();
      },
      "asDom.getDataOfNode"(node, prop) {
        // assembly/index/getDataOfNode(i32, ~lib/string/String) => ~lib/string/String
        prop = __liftString(prop >>> 0);
        return __lowerString(asDom.getDataOfNode(node, prop)) || __notnull();
      },
      "asDom.setTimeout"(key, time, params) {
        // assembly/index/setTimeout(~lib/string/String, f64, ~lib/array/Array<i32>) => i32
        key = __liftString(key >>> 0);
        params = __liftArray(__getI32, 2, params >>> 0);
        return asDom.setTimeout(key, time, params);
      },
      "asDom.setPropIntOfNode"(node, key, value) {
        // assembly/index/setPropIntOfNode(i32, ~lib/string/String, f64) => void
        key = __liftString(key >>> 0);
        asDom.setPropIntOfNode(node, key, value);
      },
      "asDom.getPropOfNode"(node, prop) {
        // assembly/index/getPropOfNode(i32, ~lib/string/String) => ~lib/string/String
        prop = __liftString(prop >>> 0);
        return __lowerString(asDom.getPropOfNode(node, prop)) || __notnull();
      },
      "asDom.getNode"(selector) {
        // assembly/index/getNode(~lib/string/String) => i32
        selector = __liftString(selector >>> 0);
        return asDom.getNode(selector);
      },
      "asDom.getCheckOfNode"(node) {
        // assembly/index/getCheckOfNode(i32) => bool
        return asDom.getCheckOfNode(node) ? 1 : 0;
      },
      "asDom.getNodes"(selector) {
        // assembly/index/getNodes(~lib/string/String) => ~lib/array/Array<i32>
        selector = __liftString(selector >>> 0);
        return __lowerArray(__setU32, 5, 2, asDom.getNodes(selector)) || __notnull();
      },
      "asDom.addEvent"(node, ev, key) {
        // assembly/index/addEvent(i32, ~lib/string/String, ~lib/string/String) => void
        ev = __liftString(ev >>> 0);
        key = __liftString(key >>> 0);
        asDom.addEvent(node, ev, key);
      },
    }),
  };
  const { exports } = await WebAssembly.instantiate(module, adaptedImports);
  const memory = exports.memory || imports.env.memory;
  const adaptedExports = Object.setPrototypeOf({
    main(pokers) {
      // assembly/index/main(~lib/string/String) => void
      pokers = __lowerString(pokers) || __notnull();
      exports.main(pokers);
    },
  }, exports);
  function __liftString(pointer) {
    if (!pointer) return null;
    const
      end = pointer + new Uint32Array(memory.buffer)[pointer - 4 >>> 2] >>> 1,
      memoryU16 = new Uint16Array(memory.buffer);
    let
      start = pointer >>> 1,
      string = "";
    while (end - start > 1024) string += String.fromCharCode(...memoryU16.subarray(start, start += 1024));
    return string + String.fromCharCode(...memoryU16.subarray(start, end));
  }
  function __lowerString(value) {
    if (value == null) return 0;
    const
      length = value.length,
      pointer = exports.__new(length << 1, 2) >>> 0,
      memoryU16 = new Uint16Array(memory.buffer);
    for (let i = 0; i < length; ++i) memoryU16[(pointer >>> 1) + i] = value.charCodeAt(i);
    return pointer;
  }
  function __liftArray(liftElement, align, pointer) {
    if (!pointer) return null;
    const
      dataStart = __getU32(pointer + 4),
      length = __dataview.getUint32(pointer + 12, true),
      values = new Array(length);
    for (let i = 0; i < length; ++i) values[i] = liftElement(dataStart + (i << align >>> 0));
    return values;
  }
  function __lowerArray(lowerElement, id, align, values) {
    if (values == null) return 0;
    const
      length = values.length,
      buffer = exports.__pin(exports.__new(length << align, 1)) >>> 0,
      header = exports.__pin(exports.__new(16, id)) >>> 0;
    __setU32(header + 0, buffer);
    __dataview.setUint32(header + 4, buffer, true);
    __dataview.setUint32(header + 8, length << align, true);
    __dataview.setUint32(header + 12, length, true);
    for (let i = 0; i < length; ++i) lowerElement(buffer + (i << align >>> 0), values[i]);
    exports.__unpin(buffer);
    exports.__unpin(header);
    return header;
  }
  function __notnull() {
    throw TypeError("value must not be null");
  }
  let __dataview = new DataView(memory.buffer);
  function __setU32(pointer, value) {
    try {
      __dataview.setUint32(pointer, value, true);
    } catch {
      __dataview = new DataView(memory.buffer);
      __dataview.setUint32(pointer, value, true);
    }
  }
  function __getI32(pointer) {
    try {
      return __dataview.getInt32(pointer, true);
    } catch {
      __dataview = new DataView(memory.buffer);
      return __dataview.getInt32(pointer, true);
    }
  }
  function __getU32(pointer) {
    try {
      return __dataview.getUint32(pointer, true);
    } catch {
      __dataview = new DataView(memory.buffer);
      return __dataview.getUint32(pointer, true);
    }
  }
  return adaptedExports;
}
export const {
  memory,
  clean,
  random,
  movePoker,
  makeMonth,
  play,
  main,
} = await (async url => instantiate(
  await (async () => {
    try { return await globalThis.WebAssembly.compileStreaming(globalThis.fetch(url)); }
    catch { return globalThis.WebAssembly.compile(await (await import("node:fs/promises")).readFile(url)); }
  })(), {
  }
))(new URL("release.wasm", import.meta.url));
