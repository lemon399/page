if (!window["__start__"]) {
  window["__start__"] = [];
}
window["__start__"].push({
  start: "body src defer",
  time: Date.now(),
  html: !!document.documentElement,
  head: !!document.head,
  body: !!document.body,
});
