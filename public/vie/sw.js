/**
 * Copyright 2018 Google Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// If the loader is already loaded, just stop.
if (!self.define) {
  let registry = {};

  // Used for `eval` and `importScripts` where we can't get script URL by other means.
  // In both cases, it's safe to use a global var because those functions are synchronous.
  let nextDefineUri;

  const singleRequire = (uri, parentUri) => {
    uri = new URL(uri + ".js", parentUri).href;
    return registry[uri] || (
      
        new Promise(resolve => {
          if ("document" in self) {
            const script = document.createElement("script");
            script.src = uri;
            script.onload = resolve;
            document.head.appendChild(script);
          } else {
            nextDefineUri = uri;
            importScripts(uri);
            resolve();
          }
        })
      
      .then(() => {
        let promise = registry[uri];
        if (!promise) {
          throw new Error(`Module ${uri} didn’t register its module`);
        }
        return promise;
      })
    );
  };

  self.define = (depsNames, factory) => {
    const uri = nextDefineUri || ("document" in self ? document.currentScript.src : "") || location.href;
    if (registry[uri]) {
      // Module is already loading or loaded.
      return;
    }
    let exports = {};
    const require = depUri => singleRequire(depUri, uri);
    const specialDeps = {
      module: { uri },
      exports,
      require
    };
    registry[uri] = Promise.all(depsNames.map(
      depName => specialDeps[depName] || require(depName)
    )).then(deps => {
      factory(...deps);
      return exports;
    });
  };
}
define(['./workbox-c6c6fb7c'], (function (workbox) { 'use strict';

  self.addEventListener('message', event => {
    if (event.data && event.data.type === 'SKIP_WAITING') {
      self.skipWaiting();
    }
  });

  /**
   * The precacheAndRoute() method efficiently caches and responds to
   * requests for URLs in the manifest.
   * See https://goo.gl/S9QRab
   */
  workbox.precacheAndRoute([{
    "url": "assets/index-bsy323rl.js",
    "revision": null
  }, {
    "url": "assets/index-c0a9854q.js",
    "revision": null
  }, {
    "url": "assets/index-c4wq49ft.css",
    "revision": null
  }, {
    "url": "assets/index-cba6l8nz.js",
    "revision": null
  }, {
    "url": "assets/index-cmpatqgp.js",
    "revision": null
  }, {
    "url": "assets/index-d9epq4zi.js",
    "revision": null
  }, {
    "url": "assets/index-ddcvpgxl.js",
    "revision": null
  }, {
    "url": "assets/index-de30uavr.js",
    "revision": null
  }, {
    "url": "assets/index-dtamsbf8.js",
    "revision": null
  }, {
    "url": "assets/index-e7av2r5q.js",
    "revision": null
  }, {
    "url": "assets/index-e8105dmd.js",
    "revision": null
  }, {
    "url": "assets/index-es8puf4s.js",
    "revision": null
  }, {
    "url": "assets/index-evjrtnsk.js",
    "revision": null
  }, {
    "url": "assets/index-fccp4707.js",
    "revision": null
  }, {
    "url": "assets/index-feglmu86.js",
    "revision": null
  }, {
    "url": "assets/index-gbyumfsc.js",
    "revision": null
  }, {
    "url": "assets/index-h8uw8ja8.js",
    "revision": null
  }, {
    "url": "assets/index-ha0pbb8e.js",
    "revision": null
  }, {
    "url": "assets/index-hem4rba4.js",
    "revision": null
  }, {
    "url": "assets/index-htawfrsw.js",
    "revision": null
  }, {
    "url": "assets/index-hzaue02t.js",
    "revision": null
  }, {
    "url": "assets/index-ijvnjyy8.js",
    "revision": null
  }, {
    "url": "assets/index-islc55tk.js",
    "revision": null
  }, {
    "url": "assets/index-jud8cevk.js",
    "revision": null
  }, {
    "url": "assets/index-kfripwlh.js",
    "revision": null
  }, {
    "url": "assets/index-kkjyvhhs.js",
    "revision": null
  }, {
    "url": "assets/index-llpqxp0s.js",
    "revision": null
  }, {
    "url": "assets/index-lwfnog6b.js",
    "revision": null
  }, {
    "url": "assets/index-n6v3xy2i.js",
    "revision": null
  }, {
    "url": "assets/index-nd22vvy9.js",
    "revision": null
  }, {
    "url": "assets/index-nuwggr2p.js",
    "revision": null
  }, {
    "url": "assets/index-ulna8t4w.js",
    "revision": null
  }, {
    "url": "assets/index-xqbapem1.js",
    "revision": null
  }, {
    "url": "assets/mdui-litqbjom.js",
    "revision": null
  }, {
    "url": "assets/react-pba7h4yg.js",
    "revision": null
  }, {
    "url": "assets/vendor-ckxrui0b.js",
    "revision": null
  }, {
    "url": "index.html",
    "revision": "bbc0e82e20a82bd1ede5be58e4127e12"
  }, {
    "url": "favicon.ico",
    "revision": "43e258387029329dc22d7670cf9ba6a9"
  }, {
    "url": "favicon.png",
    "revision": "246d954a302b7644cc9622b901bac1d4"
  }, {
    "url": "manifest.webmanifest",
    "revision": "f0470131942cc9941d3afc22528bb7e2"
  }], {});
  workbox.cleanupOutdatedCaches();
  workbox.registerRoute(new workbox.NavigationRoute(workbox.createHandlerBoundToURL("index.html")));

}));
