if (!window["__start__"]) {
  window["__start__"] = [];
}
window["__start__"].push({
  start: "body src async",
  time: Date.now(),
  html: !!document.documentElement,
  head: !!document.head,
  body: !!document.body,
});
