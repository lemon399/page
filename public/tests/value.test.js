describe("存储值", function () {
  before("支持", function () {
    if (
      typeof GM_getValue != "function" ||
      typeof GM_setValue != "function" ||
      typeof GM_listValues != "function" ||
      typeof GM_deleteValue != "function"
    ) {
      this.skip();
    }
  });
  beforeEach(function () {
    ["lemon_$test$_1", "lemon_$test$_2"].forEach((key) => {
      GM_deleteValue(key);
    });
  });
  it("未定义", async function () {
    const um = document.querySelector("#um").value;
    chai.assert.isUndefined(
      GM_getValue("lemon_$test$_1"),
      "未定义应为 undefined",
    );
    chai.assert.strictEqual(
      GM_getValue("lemon_$test$_1", 100),
      100,
      "未定义应返回默认值",
    );
    GM_deleteValue("lemon_$test$_1");
    chai.assert.strictEqual(
      GM_getValue("lemon_$test$_1", 100),
      100,
      "删除则未定义，应返回默认值",
    );
    // 脚本猫是异步，其它不必 await
    await GM_setValue("lemon_$test$_1", undefined);
    // 只有篡改猴如此
    if (um === "tm") {
      chai.assert.isUndefined(
        GM_getValue("lemon_$test$_1", 100),
        "赋值 undefined 并非未定义",
      );
    } else {
      chai.assert.strictEqual(
        GM_getValue("lemon_$test$_1", 100),
        100,
        "赋值 undefined 等同于删除，应返回默认值",
      );
    }
  });
  it("GM_setValue / GM_getValue", async function () {
    const um = document.querySelector("#um").value;
    const ret = GM_setValue("lemon_$test$_1", 10);
    if (um === "sc") {
      chai.assert.instanceOf(ret, Promise, "返回 Promise");
      chai.assert.isBoolean(await ret, "兑现布尔");
    } else {
      chai.assert.isUndefined(ret, "返回 undefined");
    }
    chai.assert.strictEqual(
      GM_getValue("lemon_$test$_1"),
      10,
      "未返回设置的值",
    );
    chai.assert.notStrictEqual(
      GM_getValue("lemon_$test$_1", 100),
      100,
      "已设置的值不应返回默认值",
    );
  });
  it("GM_listValues / GM_deleteValue", async function () {
    const um = document.querySelector("#um").value;
    chai.assert.isArray(GM_listValues(), "GM_listValues 返回数组");
    ["lemon_$test$_1", "lemon_$test$_2"].forEach((key) => {
      chai.assert.notInclude(GM_listValues(), key, "初始化不通过");
    });
    await GM_setValue("lemon_$test$_1", 1);
    chai.assert.include(GM_listValues(), "lemon_$test$_1", "应包含定义的 key");
    await GM_setValue("lemon_$test$_2", 1);
    chai.assert.include(GM_listValues(), "lemon_$test$_1", "应包含定义的 key");
    chai.assert.include(GM_listValues(), "lemon_$test$_2", "应包含定义的 key");
    GM_deleteValue("lemon_$test$_1");
    chai.assert.notInclude(
      GM_listValues(),
      "lemon_$test$_1",
      "不应包含未定义的 key",
    );
    chai.assert.include(GM_listValues(), "lemon_$test$_2", "应包含定义的 key");
    // 篡改猴：不删除值，不返回默认值
    // 暴力猴：不删除值，返回默认值
    // 脚本猫：删除值，返回默认值
    if (um === "sc") {
      await GM_setValue("lemon_$test$_2", undefined);
      chai.assert.notInclude(
        GM_listValues(),
        "lemon_$test$_2",
        "不应包含未定义的 key",
      );
    }
    GM_deleteValue("lemon_$test$_2");
    chai.assert.notInclude(
      GM_listValues(),
      "lemon_$test$_2",
      "不应包含未定义的 key",
    );
  });
  describe("存储不同类型", function () {
    beforeEach(function () {
      GM_deleteValue("lemon_$test$_1");
    });
    it("undefined", function () {
      GM_setValue("lemon_$test$_1", void 0);
      chai.assert.isUndefined(GM_getValue("lemon_$test$_1"), "应为 undefined");
    });
    it("null", function () {
      GM_setValue("lemon_$test$_1", null);
      chai.assert.isNull(GM_getValue("lemon_$test$_1"), "应为 null");
    });
    it("boolean", function () {
      GM_setValue("lemon_$test$_1", true);
      chai.assert.strictEqual(GM_getValue("lemon_$test$_1"), true, "应为 true");
    });
    it("string", function () {
      GM_setValue("lemon_$test$_1", "ok");
      chai.assert.strictEqual(GM_getValue("lemon_$test$_1"), "ok", '应为 "ok"');
    });
    it("number", function () {
      GM_setValue("lemon_$test$_1", 123);
      chai.assert.strictEqual(GM_getValue("lemon_$test$_1"), 123, "应为 123");
    });
    it("NaN", function () {
      GM_setValue("lemon_$test$_1", NaN);
      chai.assert.isNaN(GM_getValue("lemon_$test$_1"), "应为 NaN");
    });
    it("Infinity", function () {
      GM_setValue("lemon_$test$_1", Infinity);
      chai.assert.strictEqual(
        GM_getValue("lemon_$test$_1"),
        Infinity,
        "应为 Infinity",
      );
    });
    it("-Infinity", function () {
      GM_setValue("lemon_$test$_1", -Infinity);
      chai.assert.strictEqual(
        GM_getValue("lemon_$test$_1"),
        -Infinity,
        "应为 -Infinity",
      );
    });
    it("new Number", function () {
      const um = document.querySelector("#um").value;
      GM_setValue("lemon_$test$_1", new Number(10));
      if (um === "sc") {
        chai.assert.strictEqual(GM_getValue("lemon_$test$_1"), 10, "会返回 10");
      } else {
        chai.assert.deepEqual(GM_getValue("lemon_$test$_1"), {}, "会返回 {}");
      }
    });
    it("bigint", function () {
      const um = document.querySelector("#um").value;
      switch (um) {
        case "tm":
          GM_setValue("lemon_$test$_1", 12345678987654321n);
          chai.assert.isUndefined(
            GM_getValue("lemon_$test$_1"),
            "会返回 undefined",
          );
          break;
        case "vm":
          chai.assert.throws(
            GM_setValue.bind(this, "lemon_$test$_1", 12345678987654321n),
            "Do not know how to serialize a BigInt",
            "会抛错误",
          );
          break;
        case "sc":
          GM_setValue("lemon_$test$_1", 12345678987654321n);
          chai.assert.strictEqual(
            GM_getValue("lemon_$test$_1"),
            12345678987654321n,
            "应为 12345678987654321n",
          );
          break;
      }
    });
    it("function", function () {
      const um = document.querySelector("#um").value;
      const fn = () => 0;
      GM_setValue("lemon_$test$_1", fn);
      switch (um) {
        case "tm":
          chai.assert.isUndefined(
            GM_getValue("lemon_$test$_1"),
            "应返回 undefined",
          );
          break;
        case "vm":
          chai.assert.strictEqual(
            GM_getValue("lemon_$test$_1"),
            "undefined",
            "应为字符串 'undefined'",
          );
          break;
        case "sc":
          chai.assert.strictEqual(
            GM_getValue("lemon_$test$_1"),
            fn,
            "应为相同的函数",
          );
          break;
      }
    });
    it("Object", function () {
      GM_setValue("lemon_$test$_1", { a: false, b: 99, c: { d: null } });
      chai.assert.deepEqual(
        GM_getValue("lemon_$test$_1"),
        { a: false, b: 99, c: { d: null } },
        "应返回一致的对象",
      );
    });
    it("Map", function () {
      GM_setValue(
        "lemon_$test$_1",
        new Map([
          [1, true],
          [2, "ok"],
          ["null", void 0],
          [{ a: 3 }, 0],
        ]),
      );
      chai.assert.deepEqual(GM_getValue("lemon_$test$_1"), {}, "会返回 {}");
    });
    it("Array", function () {
      GM_setValue("lemon_$test$_1", [30, void 0, false, "ok", [{}, null]]);
      chai.assert.deepEqual(
        GM_getValue("lemon_$test$_1"),
        [30, null, false, "ok", [{}, null]],
        "会返回一致的数组，但 undefined 值会变为 null",
      );
    });
    it("TypedArray", function () {
      GM_setValue("lemon_$test$_1", new Uint8Array([0xc0, 0xff, 0xee, 0x5]));
      chai.assert.deepEqual(
        GM_getValue("lemon_$test$_1"),
        { 0: 192, 1: 255, 2: 238, 3: 5 },
        "会返回类数组、无 length 的对象",
      );
    });
    it("Set", function () {
      GM_setValue(
        "lemon_$test$_1",
        new Set([30, void 0, false, "ok", [{}, null]]),
      );
      chai.assert.deepEqual(GM_getValue("lemon_$test$_1"), {}, "会返回 {}");
    });
    it("Symbol", function () {
      const um = document.querySelector("#um").value;
      const sym = Symbol("ok");
      GM_setValue("lemon_$test$_1", sym);
      switch (um) {
        case "tm":
          chai.assert.isUndefined(
            GM_getValue("lemon_$test$_1"),
            "应返回 undefined",
          );
          break;
        case "vm":
          chai.assert.strictEqual(
            GM_getValue("lemon_$test$_1"),
            "undefined",
            "应为字符串 'undefined'",
          );
          break;
        case "sc":
          chai.assert.strictEqual(
            GM_getValue("lemon_$test$_1"),
            sym,
            "应为相同的 Symbol",
          );
          break;
      }
    });
    it("RegExp", function () {
      GM_setValue("lemon_$test$_1", /^ok?.$/);
      chai.assert.deepEqual(GM_getValue("lemon_$test$_1"), {}, "会返回 {}");
    });
  });
});

describe("存储值监听", function () {
  before("支持", function () {
    if (
      typeof GM_deleteValue != "function" ||
      typeof GM_setValue != "function" ||
      typeof GM_addValueChangeListener != "function" ||
      typeof GM_removeValueChangeListener != "function"
    ) {
      this.skip();
    }
  });
  beforeEach(async function () {
    GM_deleteValue("lemon_$test$_1");
    await GM_setValue("lemon_$test$_2", 10);
  });
  it("监听器 ID", function () {
    const um = document.querySelector("#um").value;
    const id = GM_addValueChangeListener("lemon_$test$_1", () => 0);
    GM_removeValueChangeListener(id);
    if (um === "vm") {
      chai.assert.isString(id, "监听器 ID 应为字符串");
    } else {
      chai.assert.isNumber(id, "监听器 ID 应为数字");
    }
  });
  it("修改值", async function () {
    const um = document.querySelector("#um").value;
    const callback = sinon.spy();
    sinon.assert.notCalled(callback);
    const id = GM_addValueChangeListener("lemon_$test$_2", callback);
    sinon.assert.notCalled(callback);
    await GM_setValue("lemon_$test$_2", true);
    callback.getCalls().forEach((call, i) => {
      cbd("vcl 修改值 " + (i + 1), call.thisValue, ...call.args);
    });
    sinon.assert.calledOnce(callback);
    if (um === "sc") {
      sinon.assert.calledWith(
        callback,
        "lemon_$test$_2",
        10,
        true,
        false,
        sinon.match.number,
      );
    } else {
      sinon.assert.calledWith(callback, "lemon_$test$_2", 10, true, false);
    }
    GM_removeValueChangeListener(id);
    await GM_setValue("lemon_$test$_2", 10);
    sinon.assert.calledOnce(callback);
    if (um === "tm") {
      chai.assert.hasAllKeys(
        callback.firstCall.thisValue,
        ["id", "key", "cb"],
        "监听回调的 this 为 { id, key, cb }",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.id,
        id,
        "this.id 应为监听器 ID",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.key,
        "lemon_$test$_2",
        "this.key 应为被监听值的 key",
      );
      chai.assert.isFunction(
        callback.firstCall.thisValue.cb,
        "this.cb 应为回调函数",
      );
    }
    if (um === "sc") {
      chai.assert.hasAllKeys(
        callback.firstCall.thisValue,
        ["name", "listener"],
        "监听回调的 this 为 { name, listener }",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.name,
        "lemon_$test$_2",
        "this.name 应为被监听值的 key",
      );
      chai.assert.isFunction(
        callback.firstCall.thisValue.listener,
        "this.listener 应为回调函数",
      );
    }
  });
  it("新增值", async function () {
    const um = document.querySelector("#um").value;
    const callback = sinon.spy();
    sinon.assert.notCalled(callback);
    const id = GM_addValueChangeListener("lemon_$test$_1", callback);
    sinon.assert.notCalled(callback);
    await GM_setValue("lemon_$test$_1", "ok");
    callback.getCalls().forEach((call, i) => {
      cbd("vcl 新增值 " + (i + 1), call.thisValue, ...call.args);
    });
    sinon.assert.calledOnce(callback);
    if (um === "sc") {
      sinon.assert.calledWith(
        callback,
        "lemon_$test$_1",
        undefined,
        "ok",
        false,
        sinon.match.number,
      );
    } else {
      sinon.assert.calledWith(
        callback,
        "lemon_$test$_1",
        undefined,
        "ok",
        false,
      );
    }
    GM_removeValueChangeListener(id);
    if (um === "tm") {
      chai.assert.hasAllKeys(
        callback.firstCall.thisValue,
        ["id", "key", "cb"],
        "监听回调的 this 为 { id, key, cb }",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.id,
        id,
        "this.id 应为监听器 ID",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.key,
        "lemon_$test$_1",
        "this.key 应为被监听值的 key",
      );
      chai.assert.isFunction(
        callback.firstCall.thisValue.cb,
        "this.cb 应为回调函数",
      );
    }
    if (um === "sc") {
      chai.assert.hasAllKeys(
        callback.firstCall.thisValue,
        ["name", "listener"],
        "监听回调的 this 为 { name, listener }",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.name,
        "lemon_$test$_1",
        "this.name 应为被监听值的 key",
      );
      chai.assert.isFunction(
        callback.firstCall.thisValue.listener,
        "this.listener 应为回调函数",
      );
    }
  });
  it("删除值", function () {
    const um = document.querySelector("#um").value;
    // 脚本猫触发变更回调过晚 暂时跳过
    if (um === "sc") this.skip();
    const callback = sinon.spy();
    sinon.assert.notCalled(callback);
    const id = GM_addValueChangeListener("lemon_$test$_2", callback);
    sinon.assert.notCalled(callback);
    GM_deleteValue("lemon_$test$_2");
    callback.getCalls().forEach((call, i) => {
      cbd("vcl 删除值 " + (i + 1), call.thisValue, ...call.args);
    });
    sinon.assert.calledOnce(callback);
    sinon.assert.calledWith(callback, "lemon_$test$_2", 10, undefined, false);
    GM_removeValueChangeListener(id);
    if (um === "tm") {
      chai.assert.hasAllKeys(
        callback.firstCall.thisValue,
        ["id", "key", "cb"],
        "监听回调的 this 为 { id, key, cb }",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.id,
        id,
        "this.id 应为监听器 ID",
      );
      chai.assert.strictEqual(
        callback.firstCall.thisValue.key,
        "lemon_$test$_2",
        "this.key 应为被监听值的 key",
      );
      chai.assert.isFunction(
        callback.firstCall.thisValue.cb,
        "this.cb 应为回调函数",
      );
    }
  });
});
