describe("脚本资源 v4", function () {
  before("支持", function () {
    if (typeof GM != "object") {
      this.skip();
    }
  });
  describe("文本内容", function () {
    before("支持", function () {
      if (typeof GM.getResourceText != "function") {
        this.skip();
      }
    });
    it("常规", async function () {
      const um = document.querySelector("#um").value;
      let text = "";
      const result = GM.getResourceText("ts");
      // 篡改猴是异步
      if (um === "tm") {
        chai.assert.typeOf(result.then, "function", "应为 Promise");
        text = await result;
      } else {
        chai.assert.notInstanceOf(result, Promise, "不应返回 Promise");
        text = result;
      }
      chai.assert.include(text, ".hitokoto", "应返回资源 ts 的内容");
    });
    it("正确 SRI", async function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoSRI"] || um !== "tm") {
        this.skip();
      } else {
        chai.assert.include(
          await GM.getResourceText("rts"),
          "MonkeyExposerNoSRI",
          "应返回资源 ts 的内容",
        );
      }
    });
    it("错误 SRI", async function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoSRI"] || um !== "tm") {
        this.skip();
      } else {
        chai.assert.isEmpty(await GM.getResourceText("rte"), "应返回空字符串");
      }
    });
    it("资源不存在", async function () {
      const um = document.querySelector("#um").value;
      if (um === "tm") {
        chai.assert.isNull(await GM.getResourceText("nts"), "应返回 null");
      } else {
        chai.assert.isUndefined(
          await GM.getResourceText("nts"),
          "应返回 undefined",
        );
      }
    });
    it("data:", async function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoData"] || um === "vm") this.skip();
      chai.assert.include(
        await GM.getResourceText("rd"),
        "reqData",
        "应返回资源 ts 的内容",
      );
    });
    it("data: base64", async function () {
      if (window["MonkeyExposerNoData"]) this.skip();
      chai.assert.include(
        await GM.getResourceText("rb"),
        "reqDataBase64",
        "应返回资源 ts 的内容",
      );
    });
  });
  // 篡改猴 Url 异步
  // 暴力猴 Url 异步 URL 同步
  // 脚本猫 URL 同步
  describe("GM.getResourceURL", function () {
    before("支持", function () {
      if (typeof GM.getResourceURL != "function") {
        this.skip();
      }
    });
    it("常规", function () {
      const url = GM.getResourceURL("ts");
      chai.assert.notInstanceOf(url, Promise, "不应为 Promise");
      chai.assert.match(url, /^(data|blob):.+/, "应返回 data: 或 blob: 地址");
    });
    it("blob:", function () {
      chai.assert.match(
        GM.getResourceURL("ts", true),
        /^blob:.+/,
        "应返回 blob: 地址",
      );
    });
    it("data:", function () {
      chai.assert.match(
        GM.getResourceURL("ts", false),
        /^data:.+/,
        "应返回 data: 地址",
      );
    });
    it("资源不存在", function () {
      chai.assert.isUndefined(GM.getResourceURL("nts"), "应返回 undefined");
    });
  });
  describe("GM.getResourceUrl", function () {
    before("支持", function () {
      if (typeof GM.getResourceUrl != "function") {
        this.skip();
      }
    });
    it("常规", async function () {
      const url = GM.getResourceUrl("ts");
      chai.assert.typeOf(url.then, "function", "应为 Promise");
      chai.assert.match(
        await url,
        /^(data|blob):.+/,
        "应返回 data: 或 blob: 地址",
      );
    });
    it("blob:", async function () {
      const um = document.querySelector("#um").value;
      if (um === "tm") this.skip();
      chai.assert.match(
        await GM.getResourceUrl("ts", true),
        /^blob:.+/,
        "应返回 blob: 地址",
      );
    });
    it("data:", async function () {
      const um = document.querySelector("#um").value;
      if (um === "tm") this.skip();
      chai.assert.match(
        await GM.getResourceUrl("ts", false),
        /^data:.+/,
        "应返回 data: 地址",
      );
    });
    it("正确 SRI", async function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoSRI"] || um !== "tm") {
        this.skip();
      } else {
        chai.assert.match(
          await GM.getResourceUrl("rts"),
          /^(data|blob):.+/,
          "应返回 data: 或 blob: 地址",
        );
      }
    });
    it("错误 SRI", async function () {
      const um = document.querySelector("#um").value;
      if (window["MonkeyExposerNoSRI"] || um !== "tm") {
        this.skip();
      } else {
        chai.assert.isEmpty(await GM.getResourceUrl("rte"), "应返回空字符串");
      }
    });
    it("资源不存在", async function () {
      const um = document.querySelector("#um").value;
      if (um === "tm") {
        chai.assert.isNull(await GM.getResourceUrl("nts"), "应返回 null");
      } else {
        chai.assert.isUndefined(
          await GM.getResourceUrl("nts"),
          "应返回 undefined",
        );
      }
    });
  });
});
