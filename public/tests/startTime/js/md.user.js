// ==UserScript==
// @name         no @run-at
// @namespace    https://viayoo.com/
// @version      0.5.2
// @description  try to take over the world!
// @author       You
// @match        https://lemon399.gitlab.io/page/tests/startTime/index.html
// @homepageURL  https://lemon399.gitlab.io/page/tests/startTime/index.html
// @grant        unsafeWindow
// @grant        GM_xmlhttpRequest
// ==/UserScript==

(function () {
  try {
    if (!unsafeWindow["__start__"]) {
      unsafeWindow["__start__"] = [];
    }
    unsafeWindow["__start__"].push({
      start: "no @run-at",
      time: Date.now(),
      html: !!document.documentElement,
      head: !!document.head,
      body: !!document.body,
    });
  } catch (e) {
    alert(e);
  }
})();
